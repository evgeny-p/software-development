Настройка среды разработки
==========================

Операционная система (ОС) — это ваша среда разработки. Для выполнения лабораторных
работ курса вам необходимо установить следующий софт:

1. Компилятор (GCC)
2. Текстовый редактор (VS Code)
3. Автоформатер кода (Clang Format)
4. Система контроля веркий (Git)
5. Система сборки (Make)

Компилятор и текстовый редактор могут быть другими, для простоты мы будем
рассматривать GCС и VS Code как самые популярные варианты.

Весь материал курса излагается в контексте командной строки Unix. Независимо от
того, какую ОС вы используете, следует установить набор стандартных unix-утилит.
Ниже приведены инструкции для Linux и Windows.


Ubuntu
------

Вы можете использовать любой дистрибутив Linux. В качестве конкретного примера
мы используем актуальный LTS-дистрибутив Ubuntu. На момент написания руководства
это
`Ubuntu 22.04.2 LTS (Jammy Jellyfish) <https://releases.ubuntu.com/jammy/>`_.

Установка базовых утилит:

.. code-block::

   $ sudo apt update
   $ sudo apt install clang-format gcc git make

После установки проверьте версии. Если вы используете другой дистрибутив,
желательно, чтобы версии утилит были не ниже:

.. code-block::

   $ clang-format --version
   Ubuntu clang-format version 14.0.0-1ubuntu1

   $ gcc --version
   gcc (Ubuntu 11.3.0-1ubuntu~22.04) 11.3.0

   $ git --version
   git version 2.34.1

   $ make --version
   GNU Make 4.3

Для установки VS Code следуйте инструкциям:
`Visual Studio Code on Linux <https://code.visualstudio.com/docs/setup/linux>`_.


Windows
-------


Установка пакетов Windows
~~~~~~~~~~~~~~~~~~~~~~~~~

Установите `App Installer из Microsoft Store <https://www.microsoft.com/p/app-installer/9nblggh4nns1#activetab=pivot:overviewtab>`_.
Вместе с ним будет установлен пакетный менеджер **winget**.

После этого откройте командную строку:

.. image:: images/env/cmd.png

Выпоните следующие команды:

.. code-block::

   winget install Git.Git
   winget install Microsoft.VisualStudioCode
   winget install LLVM.LLVM
   winget install MSYS2.MSYS2

Процесс установки всех пакетов выглядит примерно так:

.. image:: images/env/winget-install-git.png

Здесь:

1. Пакет LLVM содержит утилиту clang-format.
2. Дистрибутив MSYS2 позволит установить gcc и make.

Закройте CMD.

Добавьте в `%PATH% <https://en.wikipedia.org/wiki/PATH_(variable)>`_
пути к исполняемым файлам. Для этого:

1. Откройте настройки пользовательских переменных окружения
   (Edit environment variables for your account):

   .. image:: images/env/settings-env.png

2. Откройте окно редактирования для переменной Path:

   .. image:: images/env/environment-variables.png

3. Добавьте каталоги:
   
   .. code-block::

      C:\msys64\ucrt64\bin
      C:\msys64\usr\bin
      C:\Program Files\LLVM\bin

   .. image:: images/env/path.png


Установка пакетов MSYS2
~~~~~~~~~~~~~~~~~~~~~~~

Запустите терминал **MSYS2 UCRT64**:

.. image:: images/env/msys2-ucrt64.png

Для установки пакетов в MSYS2 используется пакетный менеджер **pacman**. Сразу
после установки следует запустить обновление командой:

.. code-block::

   $ pacman -Syu

После выполнения команды может появиться сообщение о необходимости перезапуска 
терминала:

.. code-block::

   :: To complete this update all MSYS2 processes including this terminal will be closed.
      Confirm to proceed [Y/n]

После подтверждения (y) перезапустите терминал **MSYS2 UCRT64** и повторите
команду обновления еще раз:

.. code-block::

   $ pacman -Syu

Для установки gcc и make выполните команду:

.. code-block::

   $ pacman -S mingw-w64-ucrt-x86_64-gcc make

.. image:: images/env/pacman-gcc-make.png

Закройте терминал.

Если вы все сделали правильно, то в командной строке Windows вам будут доступны
все необходимые утилиты:

.. code-block:: text

   C:\Users\csc>clang-format --version
   clang-format version 16.0.1

   C:\Users\csc>gcc --version
   gcc (Rev4, Built by MSYS2 project) 13.1.0

   C:\Users\csc>git --version
   git version 2.40.1.windows.1

   C:\Users\csc>make --version
   GNU Make 4.4.1

.. image:: images/env/cmd-versions.png


Дополнительно
~~~~~~~~~~~~~

Для удобства можете установить Windows Terminal и Far Manager:

.. code-block:: text

   winget install Microsoft.WindowsTerminal
   winget install FarManager.FarManager


Настройка VS Code
-----------------

Установите расширение 
`C/C++ for Visual Studio Code <https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools>`_.

Откройте пользовательские настройки **settings.json**:

:kbd:`Ctrl+Shift+P` :menuselection:`--> Preferences: Open User Settings (JSON)`

.. image:: images/env/open-settings-json.png

Включите в конфиге автоматическое форматирование исходников:

.. code-block:: json

   {
       "[c][cpp]": {
           "editor.formatOnSave": true,
           "editor.formatOnType": true,
           "files.insertFinalNewline": true
       }
   }
