.. _git init: https://git-scm.com/docs/git-init
.. _git config: https://git-scm.com/docs/git-config
.. _git status: https://git-scm.com/docs/git-status
.. _git log: https://git-scm.com/docs/git-log
.. _git hist: https://git-scm.com/docs/git-log
.. _git add: https://git-scm.com/docs/git-add
.. _git commit: https://git-scm.com/docs/git-commit
.. _git remote: https://git-scm.com/docs/git-remote
.. _git push: https://git-scm.com/docs/git-push
.. _git checkout: https://git-scm.com/docs/git-checkout
.. _git clone: https://git-scm.com/docs/git-clone
.. _git branch: https://git-scm.com/docs/git-branch
.. _git rebase: https://git-scm.com/docs/git-rebase
.. _git reset: https://git-scm.com/docs/git-reset
.. _git bisect: https://git-scm.com/docs/git-bisect

.. _gitignore: https://git-scm.com/docs/gitignore

.. _clang-format: http://clang.llvm.org/docs/ClangFormat.html

.. _GNU Make: https://www.gnu.org/software/make/manual/
.. _Граф зависимостей: https://en.wikipedia.org/wiki/Dependency_graph

.. _stat(2): https://linux.die.net/man/2/stat
.. _bash(1): https://linux.die.net/man/1/bash
.. _xargs(1): https://linux.die.net/man/1/xargs
.. _ssh-keygen: https://www.ssh.com/academy/ssh/keygen