.. highlight:: c


Code Style
==========

В руководство включены рекомендации из различных источников, рекомендуется
ознакомиться с оригиналами. Здесь собраны правила, нарушения которых наиболее
часто встречаются в коде студентов.


.. _vertical alignment:

Вертикальные отступы
--------------------

Рекомендуется использовать пустые строки вокруг определений функций, типов и
логических блоков кода. Допускается использование не более одной пустой строки
подряд.

.. code-block::
   :class: code-style-bad

   double calculate_distance(...)
   {
       ...
   }
   void circle_print(...)
   {
       ...
   }
   void triangle_print(...)
   {
       ...
   }


.. code-block::

   double calculate_distance(...)
   {
       ...
   }

   void circle_print(...)
   {
       ...
   }

   void triangle_print(...)
   {
       ...
   }


.. _naming:

Имена идентификаторов
---------------------

* ``lower_case_with_underscores``: переменные, функции;
* ``UpperCamelCase``: структуры, объединения, перечисления;
* ``UPPER_CASE_WITH_UNDERSCORES``: макросы.

Следует давать переменным **содержательные** имена. Различные типы объявлений
подчиняются следующим правилам:

* Имена типов и переменных должны быть существительными.
* Имена функций должны содержать глаголы.

Неудачные имена:

.. code-block::
   :class: code-style-bad

   // Слишком общее имя.
   bool flag = false;

   // Непонятно назначение буфера.
   char buf[BUFSIZE];

   // Герундий в имени функции.
   int* finding_element(const int* begin, const int* end);

   // Что конкретно проверяет эта функция?
   bool check(const Triangle* triangle);

Лучше:

.. code-block::
   :class: code-style-good

   // Название флага отражает смысл.
   bool found = false;

   // Понятно, какие данные хранятся в буфере.
   char error_message[MAX_MESSAGE_LENGTH];

   // Ок - глагол.
   int* find_element(const int* begin, const int* end);

   // Семантика функции понятна без документации.
   // Для предиката используется префикс is_, в точке вызова
   // будет очевиден тип возвращаемого значения.
   bool is_equilateral(const Triangle* triangle);


Для структур, объединений и перечислений рекомендуется создавать синонимы.

Правильно:

.. code-block::
   :class: code-style-good

   typedef struct {
       double x;
       double y;
   } Point;

   Point point;

Исключение — рекурсивные структуры:

.. code-block::

   typedef struct List {
       void* data;
       struct List* next;
   } List;

   List* list;

Неправильно:

.. code-block::
   :class: code-style-bad

   struct Point {
       double x;
       double y;
   };

   struct Point point;

Для элементов перечислений используется префикс имени перечисления:

.. code-block::
   :class: code-style-good

   typedef enum {
       HttpStatusOk = 200,
       HttpStatusBadRequest = 400,
       HttpStatusNotFound = 404,
       ...
   } HttpStatus;


.. _magic numbers:

Не используйте магические константы
-----------------------------------

Неправильно:

.. code-block::
   :class: code-style-bad

   for (int i = 0; i < 12; ++i) {
       ...
   }

Правильно:

.. code-block::

   enum { MONTHS_IN_YEAR = 12 };

   for (int i = 0; i < MONTHS_IN_YEAR; ++i) {
       ...
   }

Иногда арифметическое выражение понятнее, чем значение этого выражения.

.. code-block::
   :class: code-style-bad

   const int seconds_in_day = 86400;

Такую запись проще проверить на корректность:

.. code-block::
   
   const int seconds_in_day = 24 * 60 * 60; 

`Wiki: Magic number <https://en.wikipedia.org/wiki/Magic_number_(programming)>`_


.. _user specific code:

Не пишите user-specific код
---------------------------

Подобный код не является переносимым:

.. code-block::
   :class: code-style-bad

   FILE* dict = fopen("/home/v.pupkin/myproject/dict.txt", "r");

Лучше:

.. code-block::

   // Получение пути к файлу из внешнего источника:
   // из старнадрного потока ввода, аргументов командной строки,
   // конфигурационного файла, и т. д.
   const char* dict_file_path = ... ;

   FILE* dict = fopen(dict_file_path, "r");


.. _globals:

Не используйте глобальные переменные
------------------------------------

В подавляющем большинстве случаев глобальные переменные усложняют поддержку
кода.

Некоторые последствия использования глобальных переменных:

1. Нарушение локальности. Чем меньше область видимости отдельных элементов, тем
   проще отлаживать код.
2. Неявные зависимости. Глобальные переменные могут неявно связывать любые части
   части исходного кода.
3. Усложнение тестирования. Каждый тест должен быть независим. При наличии
   глобальных переменных каждый тест вынужден явно присваивать всем переменным
   некоторые значения для настройки стартового окружения. Наличие разделяемого
   между тестами состояния может привести к тому, что результат их выполнения
   зависит от порядка запуска.

Использование глобальных констант допустимо.

`C2 Wiki: Global Variables Are Bad <https://wiki.c2.com/?GlobalVariablesAreBad>`_


.. _const:

Использование const
-------------------

Используйте ``const`` везде, где это имеет смысл. Неизменяемые объекты упрощают
понимание программы. Рассматривайте ``const`` в сигнатурах функций как часть
контракта.

Правильно:

.. code-block::

   // Сигнатура гарантирует, что переданный объект не будет изменен.
   void print_circle(const Circle* circle);

   // Вычисленное расстояние не меняется в последующем коде.
   const double distance = calculate_distance(...);


Избыточно:

.. code-block::
   :class: code-style-bad

   // Результат может быть сохранен в изменяемую переменную.
   const double calculate_distance(...);

   // Невозможно форсировать контракт.
   double distance = calculate_distance(...);

.. code-block::
   :class: code-style-bad

   // Параметры копируются и гарантированно не будут изменены в вызывающем коде
   // даже без использования const.
   double calculate_distance(const Point point_a, const Point point_b);

`Tip of the Week #109: Meaningful const in Function Declarations <https://abseil.io/tips/109>`_


.. _short functions:

Пишите короткие функции
-----------------------

Функции должны быть короткими и решать ровно одну задачу, очевидную из имени.
По возможности старайтесь писать чистые функции.

Можно ориентироваться на следующие эвристики:

1. Функция должна умещаться на экране.
2. В функции должно быть не более 5−10 локальных переменных.
3. В функции должно быть не более 3 уровней вложенности.

`Linux kernel coding style: Functions <https://www.kernel.org/doc/html/v4.10/process/coding-style.html#functions>`_


.. _func decl:

Указывайте имена параметров в объявлениях функций
-------------------------------------------------

Имена параметров в объявлениях функций не являются обязательными для
компилятора, но значительно улучшают читаемость.

.. code-block::
   :class: code-style-bad

   // Кто кого захватывает?
   bool can_capture(Piece, Piece);

.. code-block::

   bool can_capture(Piece attacker, Piece victim);


.. _if true return true:

Избегайте избыточных ветвлений (if true return true)
----------------------------------------------------

Избыточно:

.. code-block::
   :class: code-style-bad

   bool can_capture(Piece attacker, Piece victim)
   {
       if (attacker.color != victim.color) {
           return true;
       } else {
           return false;
       }
   }

Лаконично:

.. code-block::
   :class: code-style-good

   bool can_capture(Piece attacker, Piece victim)
   {
       return attacker.color != victim.color;
   }


.. _early exits:

Используйте ранние возвраты
---------------------------

.. code-block::
   :class: code-style-bad

   if (ok) {
       call_a()
       if (a_ok) {
           call_b()
           call_c()
           if (b_ok && c_ok) {
               call_d()
               call_e()
               call_f()
               call_g()
           } else {
               throw_exception("Method A failed")
       } else {
           throw_exception("Method B or C failed")
   } else {
       throw_exception()
   }

.. code-block::

   call_a()
   if (a_error) {
      throw_exception()
   }

   call_b()
   call_c()
   if (c_error || c_error) {
       throw_exception()
   } 

   call_d()
   call_e()
   call_f()
   call_g()

`LLVM Coding Standards: Use Early Exits and continue to Simplify Code <https://llvm.org/docs/CodingStandards.html#use-early-exits-and-continue-to-simplify-code>`_
`C2 Wiki: If Ok <https://wiki.c2.com/?IfOk>`_


.. _else after return:

Не используйте else после return, break, continue
-------------------------------------------------

Не используйте ``else`` или ``else if`` после ключевых слов, прерывающих поток
выполнения команд.

.. TODO Нужен более осмысленный пример кода.

.. code-block::
   :class: code-style-bad

   void foo(int value)
   {
       int local = 0;
       for (int i = 0; i < 42; i++) {
           if (value == 1) {
               return;
           } else {
               local++;
           }

           if (value == 2) {
               continue;
           } else {
               local++;
           }
       }
   }

.. code-block::
   :class: code-style-good

   void foo(int value) {
       int local = 0;
       for (int i = 0; i < 42; i++) {
           if (value == 1) {
               return;
           }
           local++;

           if (value == 2) {
               continue;
           }
           local++;
       }
   }

`LLVM Coding Standards: Don’t use else after a return <https://llvm.org/docs/CodingStandards.html#don-t-use-else-after-a-return>`_


.. _predicates:

Выносите предикаты в функции
----------------------------

Часто в коде встречаются циклы, вычисляющие единственное булево значение.
Пример:

.. code-block::
   :class: code-style-bad

   bool found_foo = false;
   for (size_t i = 0; i  n; ++i) {
       if (is_foo(bar_list[i])) {
           found_foo = true;
           break;
       }
   }

   if (found_foo) {
       ...
   }

Вместо таких циклов рекомендуется писать функции с использованием ранних
выходов:

.. code-block::
   :class: code-style-good

   static bool contains_foo(Bar* bars, size_t n)
   {
       for (size_t i = 0; i <n; ++i) {
           if (is_foo(bars[i])) {
               return true;
           }
       }
       return false;
   }
   ...

   if (contains_foo(bars, n)) {
       ...
   }

Такой подход уменьшает уровнь вложенности и помогает вынести общий код, который
может быть переиспользован при проверке такого же предиката.

Гораздо важнее, что в этом случае вы вынуждены выбрать имя для предиката.
Польза не так очевидна на синтетическом примере, но если условие становится
более сложным, то именованный предикат позволяет понять смысл условия, не
погружаясь в детали реализации.

`LLVM Coding Standards: Turn Predicate Loops into Predicate Functions: <https://llvm.org/docs/CodingStandards.html#turn-predicate-loops-into-predicate-functions>`_


.. _include order:

Порядок заголовочных файлов
---------------------------

Предпочительный порядок заголовочных файлов:

1. Главный заголовочный файл модуля.
2. Локальные/приватные заголовочные файлы.
3. Заголовочные файлы сторонних библиотек.
4. Системные заголовочные файлы.

Блоки следует отделять пустой строкой. Файлы в каждом блоке должны быть
отсортированы лексикографически.

Абстрактный пример для circle.c:

.. code-block::

   #include "circle.h"

   #include "wkt_reader.h"

   #include <json.h>
   #include <svg.h>

   #include <math.h>
   #include <stdio.h>
   #include <stdlib.h>

`LLVM Coding Standards: #include Style <https://llvm.org/docs/CodingStandards.html#include-style>`_


.. _include less:

Подключайте как можно меньше
----------------------------

Избыточное подключение заголовочных файлов замедляет компиляцию, особенно при
подключении в другие заголовочные файлы.

Распространенная ошибка: в заголовочный файл ``circle.h`` подключается
``stdio.h``, при этом в ``circle.h`` не используются определения из ``stdio.h``:

circle.h:

.. code-block::
   :class: code-style-bad

   #pragma once

   #include <stdio.h>

   typedef struct {
       ...
   } Circle;

   void circle_print(const Circle* circle);

circle.c:

.. code-block::
   :class: code-style-bad

   #include "circle.h"

   void circle_print(const Circle* circle)
   {
       printf(...);
   }

В этом случае ``stdio.h`` следует подключать в ``circle.c``:

circle.h:

.. code-block::

   #pragma once

   typedef struct {
       ...
   } Circle;

   void circle_print(const Circle* circle);

circle.c:

.. code-block::

   #include "circle.h"

   #include <stdio.h>

   void circle_print(const Circle* circle)
   {
       printf(...);
   }


В этом примере в ``circle.h`` нужно подключить ``stdio.h``, поскольку
используется тип ``FILE``:

circle.h:

.. code-block::

   #pragma once

   #include <stdio.h>

   typedef struct {
       ...
   } Circle;

   void circle_print(const Circle* circle, FILE* stream);

`LLVM Coding Standards: #include as Little as Possible <https://llvm.org/docs/CodingStandards.html#include-as-little-as-possible>`_


.. _incapsulation:

Скрывайте детали реализации
---------------------------

Не выносите приватные объявления в заголовочный файл. Каждое публичное
определение становится частью контракта с пользователем модуля. В долгосрочной
перспективе это осложняет поддержку и рефакторинг кода.

circle.h:

.. code-block::
   :class: code-style-bad

   #pragma once

   typedef struct {
       ...
   } Circle;

   // Функция calculate_distance - деталь реализации функции circle_intersects,
   // пользователь модуля не будет вызывать ее напрямую.
   double calculate_distance(const Point* point_a, const Point* point_b);
   bool circle_intersects(const Circle* circle_a, const Circle* circle_b);


circle.c:

.. code-block::

   #include "circle.h"

   // Функцию следует определить только в файле реализации и добавить
   // спецификатор static.
   static double calculate_distance(const Point* point_a, const Point* point_b)
   {
       ...
   }


.. _encoding:

Кодировка файлов
----------------

Файлы с исходным кодом должны быть в кодировке UTF-8. Преобразование из
кодировки Windows-1251 можно выполнить с помощью утилиты ``iconv``::

  iconv -f cp1251 -t utf8 input.c > output.c

Символ перевода строки — Line Feed (LF). Для преобразования файла из формата
DOS в формат Unix используется утилита ``dos2unix``::

  dos2unix input.c


.clang-format
-------------

Код следует форматировать утилитой `clang-format`_ с конфигурационным файлом
`.clang-format <_static/.clang-format>`_:

.. literalinclude:: _static/.clang-format

.. include:: refs.rst
