Технологии разработки программного обеспечения
==============================================

.. toctree::
   :maxdepth: 1
   :caption: Содержание

   about.rst
   env.rst
   assignment.rst
   0-intro.rst
   1-git-basics.rst
   2-branches.rst
   3-build.rst
   4-unit-tests.rst
   code-style.rst

.. toctree::
   :maxdepth: 1
   :caption: Курсовой проект

   cw.rst

`Журнал успеваемости <https://docs.google.com/spreadsheets/d/e/2PACX-1vTujNdBZZrQd2p1i8v2VcGIOYf0T93XD4SVCulT91pmwMwyWVnQFczR1oB03a5VycLIFkzcsVqoIax0/pubhtml>`_


.. mermaid::

   gantt
       title Календарный план
       dateFormat YYYY-MM-DD
       axisFormat  %d.%m
       section ЛР
       ЛР0             : 2024-01-29, 1w
       ЛР1             : 2024-02-05, 4w
       ЛР2, ветки      : 2024-02-26, 2w
       ЛР3, Make, CI   : 2024-03-25, 2w
       ЛР4, Unit Tests : 2024-04-08, 2w

       section КР
       Согласование темы    : 2024-02-20, 2w
       ТЗ, План работ       : 2024-02-20, 7w
       Разработка беты      : 4w
       Доработка до релиза  : 2w
       Защита: 3w
